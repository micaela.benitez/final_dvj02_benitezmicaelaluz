﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FinalDVJ02.Managers;
using TMPro;

namespace FinalDVJ02.UI
{

    public class UIResult : MonoBehaviour
    {
        [SerializeField] private TMP_Text score;
        [SerializeField] private TMP_Text destroyedBoxes;
        [SerializeField] private TMP_Text distanceTraveled;

        private void Start()
        {
            score.text = "" + GameManager.Get().GetScore();
            destroyedBoxes.text = "Total Boxes Destroyed: " + GameManager.Get().GetDestroyedBoes();
            distanceTraveled.text = "Total Distance Traveled: " + GameManager.Get().GetDistanceTraveled();
        }

        public void LoadMainMenuScene()
        {
            LoaderManager.Get().LoadScene("MainMenu");
        }

        public void Replay()
        {
            LoaderManager.Get().LoadScene("Game");
        }

        public void ExitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}