﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FinalDVJ02.Managers;

namespace FinalDVJ02.UI
{
    public class UIMainMenu : MonoBehaviour
    {
        public void LoadGameScene()
        {
            LoaderManager.Get().LoadScene("Game");
        }

        public void LoadCreditsScene()
        {
            LoaderManager.Get().LoadScene("Credits");
        }

        public void LoadInstructionsScene()
        {
            LoaderManager.Get().LoadScene("Instructions");
        }

        public void ExitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}