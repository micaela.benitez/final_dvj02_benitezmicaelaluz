﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FinalDVJ02.Managers;

namespace FinalDVJ02.UI
{
    public class UICredits : MonoBehaviour
    {
        public void LoadMainMenuScene()
        {
            LoaderManager.Get().LoadScene("MainMenu");
        }

        public void ExitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}