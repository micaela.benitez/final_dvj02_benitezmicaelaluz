﻿using System;
using System.Collections.Generic;
using UnityEngine;
using FinalDVJ02.Managers;
using TMPro;

namespace FinalDVJ02.UI
{
    public class UIGame : MonoBehaviour
    {
        [SerializeField] private Canvas pause;

        [SerializeField] private TMP_Text timer;
        [SerializeField] private TMP_Text score;

        private void Start()
        {
            UpdateScore();
        }

        private void Update()
        {
            timer.text = "" + GameManager.Get().GetTimer().ToString("F");

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Time.timeScale = 0;
                pause.gameObject.SetActive(true);
            }
        }

        private void UpdateScore()
        {
            score.text = "" + GameManager.Get().GetScore();
        }

        private void OnEnable()
        {
            GameManager.OnUpdateScore += UpdateScore;
        }

        private void OnDisable()
        {
            GameManager.OnUpdateScore -= UpdateScore;
        }
    }
}