﻿using System;
using System.Collections.Generic;
using UnityEngine;
using FinalDVJ02.Entities;

namespace FinalDVJ02.Managers
{
    public class GameManager : MonoBehaviourSingleton<GameManager>
    {
        public static Action OnUpdateScore;

        [SerializeField] private float maxTimeInMinutes  = 1;
        private float time = 0;
        private float score = 0;
        private float distanceTraveled = 0;
        private int destroyedBoxes = 0;

        private bool gameOver = false;

        private void Update()
        {
            if (!gameOver)
                time += Time.deltaTime;

            if (time > (maxTimeInMinutes * 60))
            {
                time = 0;
                gameOver = true;
                LoaderManager.Get().LoadScene("Result");
            }
        }

        private void Init()
        {
            time = 0;
            score = 0;
            destroyedBoxes = 0;
            distanceTraveled = 0;
            gameOver = false;
        }

        private void AddPoints(float points)
        {
            score += points;
            destroyedBoxes++;
            OnUpdateScore?.Invoke();
        }

        private void CalculatingDistanceTraveled()
        {
            distanceTraveled += 0.1f;
        }

        public float GetTimer()
        {
            return time;
        }

        public float GetScore()
        {
            return score;
        }

        public int GetDestroyedBoes()
        {
            return destroyedBoxes;
        }

        public float GetDistanceTraveled()
        {
            return distanceTraveled;
        }

        private void OnEnable()
        {
            Box.OnDestroyBox += AddPoints;
            Tank.OnStartGame += Init;
            Tank.OnDistance += CalculatingDistanceTraveled;
        }

        private void OnDisable()
        {
            Box.OnDestroyBox -= AddPoints;
            Tank.OnStartGame -= Init;
            Tank.OnDistance -= CalculatingDistanceTraveled;
        }
    }
}