﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FinalDVJ02.Entities;

namespace FinalDVJ02.Managers
{
    public class BoxesManager : MonoBehaviour
    {
        [Header("Terrain data")]
        [SerializeField] private Terrain terrain;
        private float terrainWidth;
        private float terrainHeight;
        private float posX;
        private float posZ;

        [Header("Boxes data")]
        [SerializeField] private Box box1Prefab;
        [SerializeField] private List<Box.BoxData> box1;
        [SerializeField] private Box box2Prefab;
        [SerializeField] private List<Box.BoxData> box2;
        [SerializeField] private Box box3Prefab;
        [SerializeField] private List<Box.BoxData> box3;

        private void Awake()
        {
            terrainWidth = terrain.terrainData.size.x;
            terrainHeight = terrain.terrainData.size.z;
        }

        private void Start()
        {
            CreateBoxes(box1Prefab, box1);
            CreateBoxes(box2Prefab, box2);
            CreateBoxes(box3Prefab, box3);
        }

        private void CreateBoxes(Box prefab, List<Box.BoxData> box)
        {
            for (int i = 0; i < box.Count; i++)
            {
                Box.BoxData boxData = box[i];

                CalculateTerrainPosition();
                GameObject go = Instantiate(prefab, new Vector3(posX, 50, posZ), Quaternion.identity).gameObject;
                go.transform.parent = gameObject.transform;

                Box b = go.GetComponent<Box>();
                b.InitBox(boxData);
            }
        }

        private void CalculateTerrainPosition()
        {
            posX = Random.Range(0, terrainWidth);
            posZ = Random.Range(0, terrainHeight);
        }
    }
}