﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FinalDVJ02.Entities
{
    public class Box : MonoBehaviour
    {
        public static Action<float> OnDestroyBox;

        [SerializeField] private ParticleSystem explosion;

        [Serializable]
        public class BoxData
        {
            public float size;
            public float lives;
            public float points; 
        }

        private float lives;
        private float points;

        public void InitBox(BoxData boxData)
        {
            transform.localScale = Vector3.one * boxData.size;
            lives = boxData.lives;
            points = boxData.points;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Bullet")
            {
                Bullet bullet;
                bullet = FindObjectOfType<Bullet>();
                lives -= bullet.damage;

                if (lives <= 0)
                {
                    Instantiate(explosion, transform.position, Quaternion.Euler(-90, 0, 0));
                    OnDestroyBox?.Invoke(points);
                    Destroy(gameObject);
                }
            }
        }
    }
}