﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FinalDVJ02.Entities
{
    public class CameraMovement : MonoBehaviour
    {
        [SerializeField] private Transform player;
        [SerializeField] private Vector3 playerDistance;
        [SerializeField] private Vector3 playerRotation;

        private void Start()
        {
            transform.rotation = Quaternion.Euler(playerRotation.x, playerRotation.y, playerRotation.z);
            transform.position = new Vector3(player.position.x - playerDistance.x, playerDistance.y, player.position.z - playerDistance.z);
        }
    }
}