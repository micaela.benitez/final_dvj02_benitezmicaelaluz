﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FinalDVJ02.Entities
{
    public class Tank : MonoBehaviour
    {
        public static Action OnStartGame;
        public static Action OnDistance;

        [SerializeField] private Canvas pause;

        [Header("Tank Data")]
        [SerializeField] private GameObject turret;
        [SerializeField] private float movementSpeed = 3;
        [SerializeField] private float rotationSpeed = 20;
        private Rigidbody rig;

        [Header("Bullet Data")]
        [SerializeField] private GameObject bulletPrefab;
        [SerializeField] private Transform bulletContainer;

        private void Awake()
        {
            rig = GetComponent<Rigidbody>();
            OnStartGame?.Invoke();
        }

        private void FixedUpdate()
        {
            float ver = Input.GetAxis("Vertical");
            float hor = Input.GetAxis("Horizontal");

            Movement(ver);
            Rotation(hor);
        }

        private void Update()
        {
            if (!pause.gameObject.activeSelf)
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                    StartCoroutine(Shot());
            }
        }

        private void Movement(float ver)
        {
            Vector3 movement = transform.forward * ver * movementSpeed * Time.deltaTime;
            rig.MovePosition(rig.position + movement);
            OnDistance?.Invoke();
        }

        private void Rotation(float hor)
        {
            float turn = hor * rotationSpeed * Time.deltaTime;
            Quaternion turnRotation = Quaternion.Euler(0, turn, 0);
            rig.MoveRotation(rig.rotation * turnRotation);   // Con quaternions no se puede usar +, para incrementar se utiliza *
        }

        IEnumerator Shot()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                Ray newDirection = new Ray(transform.position + Vector3.up, hit.point - (transform.position + Vector3.up));
                Vector3 pos = new Vector3(newDirection.direction.x, 0, newDirection.direction.z);
                rig.MoveRotation(Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(pos.normalized), 1));
            }

            yield return new WaitForSeconds(0.1f);

            Instantiate(bulletPrefab, turret.transform.position, transform.rotation, bulletContainer);
        }
    }
}