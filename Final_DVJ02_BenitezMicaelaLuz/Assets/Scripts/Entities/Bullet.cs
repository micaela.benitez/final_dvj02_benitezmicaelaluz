﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FinalDVJ02.Entities
{
    public class Bullet : MonoBehaviour
    {
        [Header("Bullet Data")]
        public float damage;
        private Rigidbody rig;

        private void Awake()
        {
            rig = GetComponent<Rigidbody>();
        }

        private void Start()
        {
            rig.AddForce(transform.forward * 50, ForceMode.Impulse);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag != "Box")
                Destroy(gameObject);
        }  
    }
}